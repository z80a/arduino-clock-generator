#ifndef Storage_h
#define Storage_h

#include "ClockSpeed.h"

class Storage {
  typedef enum StorageAddress: int {
    ClockSpeedAddress = 1,
  };

  public:
    void writeClockSpeed(ClockSpeed_t);
    ClockSpeed_t readClockSpeed();
};

#endif
