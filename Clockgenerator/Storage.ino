#include "Storage.h"
#include <EEPROM.h>

void Storage::writeClockSpeed(ClockSpeed_t clockSpeed) {
  EEPROM.write(ClockSpeedAddress, clockSpeed);
}

ClockSpeed_t Storage::readClockSpeed() {
  int clockSpeed = EEPROM.read(ClockSpeedAddress);
  Serial.println("======");
  Serial.println(clockSpeed);
  return clockSpeed;
}
