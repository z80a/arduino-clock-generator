/*
  Clock Generator, meant for driving an Z80 CPU

  Maurice de Bijl, 2020
  Work in Progress

  Coding guidelines:
  - under_scores for plain-c variables that are in the glogal namespace
  - camelCasing for all other (like classes, variables in functions etc)

  Parts taken from https://arduino.stackexchange.com/questions/16698/arduino-constant-clock-output
*/

#include "Button.h"
#include "ClockSpeed.h"
#include "Storage.h"

typedef enum UiState : int {
  normal = 0,
  paused = 1
};

//#ifdef __AVR_ATmega2560__
//  const byte CLOCKOUT_PIN = 11;  // Mega 2560
//#else
const byte CLOCKOUT_PIN = 9;   // Uno, Duemilanove, etc.
//#endif

const byte STEPBUTTON_PIN = 5;
const byte SPEEDBUTTON_PIN = 6;

const byte RESET_PIN = 2;

Button clock_stepButton(STEPBUTTON_PIN);
Button clock_speedButton(SPEEDBUTTON_PIN);

int clock_step = 0;
Storage storage = Storage();
int clock_speed = 0;
int clock_select_direction = -1;

float clock_cycle_duration; // msec
unsigned long reset_timer;
unsigned long reset_start_time;
unsigned long reset_end_time;
bool reset_is_active;

// OLED display for feedback

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);


void fatal_error() {
  for (;;);
}

void initialize_logging() {
  Serial.begin(115200);
}

void initialize_inputs() {
  clock_stepButton.begin();
  clock_speedButton.begin();
}


void setup_oled_screen() {
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C for 128x32
    Serial.println(F("SSD1306 allocation failed"));
    fatal_error();
  }

  display.clearDisplay();
}

void setup_clockgenerator() {
  pinMode (CLOCKOUT_PIN, OUTPUT);
  // set up Timer 1
  TCCR1A = bit (COM1A0);  // toggle OC1A on Compare Match

  set_clock_speed();
}

void show_centered(String text, int x = -1, int y = -1)
{
    int x1,y1,w,h;
    display.getTextBounds(text, 0, 0, &x1, &y1, &w, &h); //calc width of new string

    if ((x == -1) && (y == -1)) {
      display.setCursor((SCREEN_WIDTH - w) / 2, (SCREEN_HEIGHT - h) / 2);
    } else if (x == -1) {
      display.setCursor((SCREEN_WIDTH - w) / 2, y);
    } else if (y == -1) {
      display.setCursor(x, (SCREEN_HEIGHT - h) / 2);
    }
    display.print(text);
}

void setup() {
  initialize_logging();

  clock_speed = storage.readClockSpeed();

  initialize_inputs();
  setup_oled_screen();
  setup_clockgenerator();
}

//void show

void show_speed(String speed) {
  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(SSD1306_WHITE);
  show_centered(speed);
  display.display();
}

void show_step(String stepName) {
  display.setTextColor(SSD1306_WHITE);

  display.clearDisplay();

  display.setTextSize(2);
  display.setCursor(0, 0);
  show_centered(F("Paused"), -1, 0);

  if (clock_step > 0) {
    display.setTextSize(2);
    show_centered(stepName, -1, 16);
  }
  display.display();
}

void set_clock_speed() {
  float divider = 0.0;
  bool shouldPrescale = false;
  String humanReadableSpeed("");

  switch (clock_speed) {
    case Clock8MHz:
      clock_cycle_duration = 1000 / 8000000;
      humanReadableSpeed = "8 MHz";
      divider = 0;
      break;
    case Clock4MHz:
      clock_cycle_duration = 1000 / 4000000;
      humanReadableSpeed = "4 MHz";
      divider = 1.0;
      break;
    case Clock2MHz:
      clock_cycle_duration = 1000 / 2000000;
      humanReadableSpeed = "2 MHz";
      divider = 4.0;
      break;
    case Clock1MHz:
      clock_cycle_duration = 1000 / 1000000;
      humanReadableSpeed = "1 MHz";
      divider = 8.0;
      break;
    case Clock500KHz:
      clock_cycle_duration = 1000 / 500000;
      humanReadableSpeed = "500 KHz";
      divider = 16.0;
      break;
    case Clock100KHz:
      clock_cycle_duration = 1000 / 100000;
      humanReadableSpeed = "100 KHz";
      divider = 80.0;
      break;
    case Clock10KHz:
      clock_cycle_duration = 1000 / 10000;
      humanReadableSpeed = "10 KHz";
      divider = 800.0;
      break;
    case Clock1000Hz:
      clock_cycle_duration = 1000 / 1000;
      humanReadableSpeed = "1000 Hz";
      divider = 8000.0;
      break;
    case Clock100Hz:
      clock_cycle_duration = 1000 / 100;
      humanReadableSpeed = "~100 Hz";
      shouldPrescale = true;
      divider = 80.0;
      break;
    case Clock20Hz:
      clock_cycle_duration = 1000 / 20;
      humanReadableSpeed = "~20 Hz";
      shouldPrescale = true;
      divider = 400.0;
      break;
    case Clock10Hz:
      clock_cycle_duration = 1000 / 10;
      humanReadableSpeed = "~10 Hz";
      shouldPrescale = true;
      divider = 800.0;
      break;
    case Clock5Hz:
      clock_cycle_duration = 1000 / 5;
      humanReadableSpeed = "~5 Hz";
      shouldPrescale = true;
      divider = 1600.0;
      break;
    case Clock1Hz:
      clock_cycle_duration = 1000 / 1;
      humanReadableSpeed = "~1 Hz";
      shouldPrescale = true;
      divider = 8000.0;
      break;
    default:
      clock_cycle_duration = 1000 / 4000000;
      humanReadableSpeed = "*4 MHz";
      divider = 1.0;
      break;
  }

  show_speed(humanReadableSpeed);
  Serial.print(humanReadableSpeed);

  Serial.print(F(", divider: "));
  Serial.println(String(divider));

  // Set clock speed
  if (shouldPrescale) {
    // CTC, prescale / 1024 (see https://circuitdigest.com/microcontroller-projects/arduino-timer-tutorial)
    TCCR1B = bit (WGM12) | bit (CS10) | bit (CS12);
  } else {
    // CTC, no prescaling
    TCCR1B = bit (WGM12) | bit (CS10);
  }
  OCR1A = divider;
}

void pause_clock() {
  TCCR1A = 0;
}

void clock_high() {
  digitalWrite(CLOCKOUT_PIN, HIGH);
}

void clock_low() {
  digitalWrite(CLOCKOUT_PIN, LOW);
}


void show_continue() {
  display.setTextSize(2);
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(0, 0);

  display.clearDisplay();
  show_centered(F("Continue"));
  display.display();
}

void show_direction() {
  display.setTextSize(2);
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(0, 0);

  display.clearDisplay();
  if (clock_select_direction > 0) {
    show_centered(F("Direction:"), -1, 0);
    show_centered(F("up"), -1, 16);
  } else {
    show_centered(F("Direction:"), -1, 0);
    show_centered(F("down"), -1, 16);
  }

  display.display();
}

void show_reset() {
  display.setTextSize(2);
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(0, 0);

  display.clearDisplay();
  show_centered(F("Resetting"));
  display.display();  
}

bool reset_pressed() {
  pinMode(RESET_PIN, INPUT);
  return !digitalRead(RESET_PIN);
}

void hold_reset() {
  pinMode(RESET_PIN, OUTPUT);
  return digitalWrite(RESET_PIN, LOW);
}

void release_reset() {
  pinMode(RESET_PIN, OUTPUT);
  return digitalWrite(RESET_PIN, HIGH);
}

void reset_helper() {

  // Release reset
  if (reset_is_active) {
    if (millis() > reset_end_time) {
      Serial.println("Reset assist stopped");
      release_reset();
      reset_is_active = false;
      set_clock_speed();
    } else {
//      hold_reset();
    }
    return;    
  }

  // Press and hold reset for enough cycles
  bool resetPressed = reset_pressed();
  if (reset_pressed() && !reset_is_active) {
    float waitTime =  (clock_cycle_duration * 3);
    
    Serial.println("Reset assist started" );
    Serial.println(waitTime);
    show_reset();
    reset_is_active = true;
    reset_start_time = millis();
    reset_end_time = reset_start_time + waitTime;
    hold_reset();
    return;
  }  
}

// the loop function runs over and over again forever
void loop() {
  reset_helper();
 
  // Pause & Step
  ButtonState buttonState = clock_stepButton.buttonState();
  if (buttonState == ShortPress) {

    pause_clock();
    if (clock_step == 0) {
      show_step("");
      clock_step += 2;

    } else {
      unsigned long stepNumber = clock_step / 2;

      if (clock_step % 2 == 0) {
        String stepName = String(stepNumber) + " high";
        show_step(stepName);
        clock_high();
      } else {
        String stepName = String(stepNumber) + " low";
        show_step(stepName);
        clock_low();
      }

      clock_step++;
    }
  }

  // Continue
  if (buttonState == LongPress) {
    TCCR1A = bit (COM1A0);
    Serial.println(F("Continued clock"));
    clock_step = 0;
    show_continue();
    delay(1000);
    set_clock_speed();
  }

  // Select next clock speed
  buttonState = clock_speedButton.buttonState();
  if (buttonState == ShortPress) {
    clock_speed += clock_select_direction;
    if (clock_speed > ClockLast) {
      clock_speed = ClockFirst;
    }

    if (clock_speed < ClockFirst) {
      clock_speed = ClockLast;
    }


    storage.writeClockSpeed(clock_speed);
    set_clock_speed();
  }

  // Change direction
  if (buttonState == LongPress) {
    clock_select_direction = clock_select_direction * -1;

    show_direction();
    delay(1000);
    set_clock_speed();
  }
}
