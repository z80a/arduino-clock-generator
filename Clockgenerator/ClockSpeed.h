#ifndef ClockSpeed_h
#define ClockSpeed_h

typedef enum ClockSpeed: int {
  ClockFirst = 0,
  Clock1Hz = 0,
  Clock5Hz = 1,
  Clock10Hz = 2,
  Clock20Hz = 3,
  Clock100Hz = 4,
  Clock1000Hz = 5,
  Clock10KHz = 6,
  Clock100KHz = 7,
  Clock500KHz = 8,
  Clock1MHz = 9,
  Clock2MHz = 10,
  Clock4MHz = 11,
  Clock8MHz = 12,
  ClockLast = 5 // skipping everything > 1000 Hz
} ClockSpeed_t;

#endif
