# Clock generator for Z80 CPU and other purposes

## Overview
I created this clock generator to drive a Z80 CPU and diagnose problems by stepping through each cycle.

![](images/on%20backplane.jpeg?inline=false)
The black module is the Z80 CPU, the white module is the clock module.

![](images/inside%20of%20module.jpeg?inline=false)
Lot's of room and pins to spare for future ideas

I spent a lot of time to create, test and publish this stuff. If it helped you or made you smile, please consider donating some spare change!

Regards,
Maurice de Bijl

[![paypal](https://www.paypalobjects.com/en_US/i/btn/btn_donate_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=AEZXY9YE6ZX7L)


## Features
- Selectable speed
- Can be paused and operated step by step (low & high signals separately)
- 128x32 I2C OLED display for showing selected speed
- Reset assist: holds reset for just enough clock cycles

## Wishlist
- Auto reset on power on
- Support for > 1000 Hz
- Showing the exact (measured) clock signal

## Dependencies
- Adafruit GFX library, https://github.com/adafruit/Adafruit-GFX-Library
- Adafruit BusIO library, https://github.com/adafruit/Adafruit_BusIO
- Adafruit SSD1306 library, https://github.com/adafruit/Adafruit_SSD1306

These are easy to install in the Arduino IDE:
- Sketch / Include Library / Manage Libraries

## BOM
- [Arduino Nano (5V) ](https://www.aliexpress.com/wholesale?SearchText=arduino+nano+5v) with header pins
- [2 push buttons 12x12x7.3mm with cap, normally open](https://www.amazon.com/Gikfun-12x12x7-3-Tactile-Momentary-Arduino/dp/B01E38OS7K/ref=sr_1_29). One is used for pausing the clock generator and stepping/continue. The other button is for changing the clockspeed.
- [SSD1306 OLED screen 128x32 I2C](https://www.aliexpress.com/wholesale?SearchText=oled+128x32+I2C+SSD1306)
- Led
- Resistor for LED (330 Ohm for red/yellow/green leds, 10K for blue/white leds)
- [RC2014 protoytype PCB](https://www.tindie.com/products/semachthemonkey/prototype-pcb-for-rc2014/)
- 90 degrees 2x40 (double!) header pin. Note: the case only fits with double header pins
- An RC2014 or similar backplane. I use the Pro: https://www.tindie.com/products/semachthemonkey/backplane-pro-for-rc2014-z80-homebrew-computer/
- Your favourite drugs (caffeine or maybe Zen meditation)
- A nice case to 3D print: https://gitlab.com/z80a/rc2014-module-cases
- Some experience with electronics, because below are not step by step instructions

## Assembling guidelines
- Print the case (it's called "Bus Module Low profile Arduino clock module*.stl")
- Cut out the supports in the window for the OLED screen
- When using the double header pins, don't forget to cut the traces between the top and bottom rows of headers (at least for pins in the upper row that are populated)
- Solder the Arduino on the PCB
- Solder a connection between pin 9 on the Arduino and the header pin for the Clock signal
- Optionally you could als solder a jumper to select between CLOCK1 or CLOCK2 on the header
- Solder a connection between pin 2 on the arduino and reset on the header
- Solder a connection between the Arduino and header for GND and +5V
- Solder the LED with the resistor to GND and +5V on the header pins
- Connect the OLED screen according to your Arduino pinout scheme: you'll need SDA (data), SCL (clock), GND and 5V or 3V (depending on your OLED screen). On an Arduino Nano SDA = A4, SCL = A5
- Solder the step and continue push buttons on a small strip of PCB, 15 mm apart (center to center) and drill holes in the PCB to match with the case
- Connect the buttons between the pins defined in the code and ground
- Glue the OLD screen in place
- Remove unused pins from the double header pin (remove everything from the upper row except 10, counted from GND)
- Solder the header pins
- Build the Arduino project and upload it

## Using it
- Press the first button (pin 5) to pause the clock
- Press it again to step through (1 clock cycle)
- Press and hold to exit (continue normal clockcycles)
- Press the second button (pin 6) to select the next clock clockspeed
- Press and hold to change the direction (from high to low or vice versa)
